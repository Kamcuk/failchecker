MAKEFLAGS = -rR

all: test

build:
	nice cmake -S . -B _build -D FAC_DEV=1 -G Ninja
	nice cmake --build _build
test: build
	ulimit -c 0 && cd _build && nice ctest --output-on-failure

debug: build
	,gdbbatchrun env LD_PRELOAD=./_build/libfailchecker.so /home/kamil/myprojects/failchecker/_build/test_1

inspect:
	cc -I _build/include/ -E src/failchecker_functions.c
