#!/bin/bash
# shellcheck disable=2016
set -euo pipefail

if (($#)); then
	mkdir -vp "$(dirname "$1")"
	exec 1>"$1"
fi

sed -n '
# Remove lines starting with //
/#/d
# Ignore lines starting with //
# Ignore empty lines
\@^ *//\|^ *$@{
	p
	d
}
# if void function, ignore
/^ *void/d
# Add ; on the end if missing.
s/;\?$/;/

/^ *\(\([^ ]\+ \+\)*[^ ]\+ *\( \|\*\)\)\([a-z_]\+\)(\(.*\));/{
	s//FAC_DEFINE(\1, \4, \5);/
}
/FAC_DEFINE(\(.*\), \(.*\), \(.*\) *, *\.\.\.);$/{
	s//FAC_DEFINE_VA_ARGS(\1, \2, \3);/
}
/, void);/{
	s//);/
}
s/$/\n/
p

' <<'EOF'

// stdlib.h
           double        atof(const char *);
           int           atoi(const char *);
           long          atol(const char *);
           long long     atoll(const char *);
           #void         *calloc(size_t, size_t);
           char         *getenv(const char *);
           int           getsubopt(char **, char *const *, char **);
           int           grantpt(int);
           char         *initstate(unsigned, char *, size_t);
		   void         *malloc(size_t);
           int           mblen(const char *, size_t);
           size_t        mbstowcs(wchar_t *restrict, const char *restrict, size_t);
           int           mbtowc(wchar_t *restrict, const char *restrict, size_t);
           char         *mkdtemp(char *);
           int           mkstemp(char *);
           int           posix_memalign(void **, size_t, size_t);
           int           posix_openpt(int);
           char         *ptsname(int);
           int           putenv(char *);
		   void         *realloc(void *, size_t);
           char         *realpath(const char *restrict, char *restrict);
           int           setenv(const char *, const char *, int);
           double        strtod(const char *restrict, char **restrict);
           float         strtof(const char *restrict, char **restrict);
           long          strtol(const char *restrict, char **restrict, int);
           long double   strtold(const char *restrict, char **restrict);
           long long     strtoll(const char *restrict, char **restrict, int);
           unsigned long strtoul(const char *restrict, char **restrict, int);
           unsigned long long strtoull(const char *restrict, char **restrict, int);
           int           system(const char *);
           int           unlockpt(int);
           size_t        wcstombs(char *restrict, const wchar_t *restrict, size_t);
           int           wctomb(char *, wchar_t);


// stdio.h
           char    *ctermid(char *);
           int      dprintf(int, const char *restrict, ...)
           int      fclose(FILE *);
           FILE    *fdopen(int, const char *);
           int      feof(FILE *);
           int      ferror(FILE *);
           int      fflush(FILE *);
           int      fgetc(FILE *);
           int      fgetpos(FILE *restrict, fpos_t *restrict);
           char    *fgets(char *restrict, int, FILE *restrict);
           int      fileno(FILE *);
           void     flockfile(FILE *);
           FILE    *fmemopen(void *restrict, size_t, const char *restrict);
           FILE    *fopen(const char *restrict, const char *restrict);
           #int      fprintf(FILE *restrict, const char *restrict, ...);
           int      fputc(int, FILE *);
           int      fputs(const char *restrict, FILE *restrict);
           size_t   fread(void *restrict, size_t, size_t, FILE *restrict);
           FILE    *freopen(const char *restrict, const char *restrict, FILE *restrict);
           int      fscanf(FILE *restrict, const char *restrict, ...);
           int      fseek(FILE *, long, int);
           int      fseeko(FILE *, off_t, int);
           int      fsetpos(FILE *, const fpos_t *);
           long     ftell(FILE *);
           off_t    ftello(FILE *);
           int      ftrylockfile(FILE *);
           void     funlockfile(FILE *);
           size_t   fwrite(const void *restrict, size_t, size_t, FILE *restrict);
           int      getc(FILE *);
           int      getchar(void);
           int      getc_unlocked(FILE *);
           int      getchar_unlocked(void);
           ssize_t  getdelim(char **restrict, size_t *restrict, int, FILE *restrict);
           ssize_t  getline(char **restrict, size_t *restrict, FILE *restrict);
           char    *gets(char *);
           FILE    *open_memstream(char **, size_t *);
           int      pclose(FILE *);
           void     perror(const char *);
           FILE    *popen(const char *, const char *);
           int      printf(const char *restrict, ...);
           int      putc(int, FILE *);
           int      putchar(int);
           int      putc_unlocked(int, FILE *);
           int      putchar_unlocked(int);
           int      puts(const char *);
           int      remove(const char *);
           int      rename(const char *, const char *);
           int      renameat(int, const char *, int, const char *);
           void     rewind(FILE *);
           int      scanf(const char *restrict, ...);
           void     setbuf(FILE *restrict, char *restrict);
           int      setvbuf(FILE *restrict, char *restrict, int, size_t);
           int      snprintf(char *restrict, size_t, const char *restrict, ...);
           int      sprintf(char *restrict, const char *restrict, ...);
           int      sscanf(const char *restrict, const char *restrict, ...);
           char    *tempnam(const char *, const char *);
           FILE    *tmpfile(void);
           int      ungetc(int, FILE *);
           int      vdprintf(int, const char *restrict, va_list);
           #int      vfprintf(FILE *restrict, const char *restrict, va_list);
           int      vfscanf(FILE *restrict, const char *restrict, va_list);
           int      vprintf(const char *restrict, va_list);
           int      vscanf(const char *restrict, va_list);
           int      vsnprintf(char *restrict, size_t, const char *restrict, va_list);
           int      vsprintf(char *restrict, const char *restrict, va_list);
           int      vsscanf(const char *restrict, const char *restrict, va_list);

// time.h
           char      *asctime(const struct tm *);
           char      *asctime_r(const struct tm *restrict, char *restrict);
           clock_t    clock(void);
           int        clock_getcpuclockid(pid_t, clockid_t *);
           int        clock_getres(clockid_t, struct timespec *);
           int        clock_gettime(clockid_t, struct timespec *);
           int        clock_nanosleep(clockid_t, int, const struct timespec *, struct timespec *);
           int        clock_settime(clockid_t, const struct timespec *);
           char      *ctime(const time_t *);
           char      *ctime_r(const time_t *, char *);
           double     difftime(time_t, time_t);
           struct tm *getdate(const char *);
           struct tm *gmtime(const time_t *);
           struct tm *gmtime_r(const time_t *restrict, struct tm *restrict);
           struct tm *localtime(const time_t *);
           struct tm *localtime_r(const time_t *restrict, struct tm *restrict);
           time_t     mktime(struct tm *);
           int        nanosleep(const struct timespec *, struct timespec *);
           size_t     strftime(char *restrict, size_t, const char *restrict, const struct tm *restrict);
           size_t     strftime_l(char *restrict, size_t, const char *restrict, const struct tm *restrict, locale_t);
           char      *strptime(const char *restrict, const char *restrict, struct tm *restrict);
           time_t     time(time_t *);
           int        timer_create(clockid_t, struct sigevent *restrict, timer_t *restrict);
           int        timer_delete(timer_t);
           int        timer_getoverrun(timer_t);
           int        timer_gettime(timer_t, struct itimerspec *);
           int        timer_settime(timer_t, int, const struct itimerspec *restrict, struct itimerspec *restrict);

EOF
