#include "failchecker.h"
#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include <errno.h>

static int errors = 0;

#define pdbg(str, ...)   fprintf(stderr, "%s:%d: " str "\n", __func__, __LINE__, ##__VA_ARGS__)
#define check(expr)  do { \
	const bool _res = !(expr); \
	if (_res) errors++; \
	pdbg("%sexpresion %s %s%s", \
			_res ? "\e[91m" : "", \
			#expr, _res ? "failed" : "ok", \
			_res ? "\e[0m" : ""); \
} while(0)

