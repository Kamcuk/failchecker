#include "test.h"
int main() {
	FAC_FOR(err) {
		void *a = malloc(1);
		if (!err) {
			check(a != NULL);
		} else {
			check(a == NULL);
			check(errno == ENOMEM);
		}
		free(a);
	}
	FAC_FOR(err) {
		void *a = malloc(1);
		void *b = malloc(1);
		if (!err) {
			check(a != NULL);
			check(b != NULL);
		} else {
			check(a == NULL || b == NULL);
		}
		free(a);
		free(b);
	}
	return errors;
}
