#include "test.h"
int main() {
	FAC_FOR(ok) {
		errno = 0;
		void *p = malloc(1);
		int i = atoi("1");
		pdbg("ok=%d errno=%d %p i=%d", (int)ok, errno, p, i);
		switch (ok) {
		case 0: check(p != NULL); check(i == 1); break;
		case 1: check(p == NULL); check(i == 1); break;
		case 2: check(p != NULL); check(i == 0 && errno != 0); break;
		}
		free(p);
	}
	return errors;
}
