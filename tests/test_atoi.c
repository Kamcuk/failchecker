#include "test.h"
int main() {
	FAC_FOR(err) {
		errno = 0;
		int i = atoi("1");
		if (!err)
			check(i == 1);
		else
			check(i == 0 && errno == ENOMEM);
	}
	return errors;
}
