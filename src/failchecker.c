#define _GNU_SOURCE  1
#include "failchecker.h"
#include <malloc.h>
#include <execinfo.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>
#include <errno.h>
#include <time.h>
#include <stdarg.h>
#include <dlfcn.h>
#include <search.h>

/* ------------------------------------------------------------------------- */

#define COUNT(arr)  (sizeof(arr)/sizeof(*arr))

#define FIND_ARR(key, arr, cmp)  \
	lfind(key, arr, (size_t[1]){ COUNT(arr) }, sizeof(*arr), cmp);

void *__libc_malloc(size_t);
void *__libc_realloc(void *, size_t);
void __libc_free(void *);

static void fac_disable(void);
static void fac_enable(void);

__attribute__((__format__(__printf__, 2, 3)))
static int fac_call_fprintf(FILE *ff, const char *str, ...) {
	fac_disable();
	va_list va;
	va_start(va, str);
	int ret = vfprintf(ff, str, va);
	va_end(va);
	fac_enable();
	return ret;
}

#define perr(...)  fac_call_fprintf(stderr, __VA_ARGS__)
#define pdbg(str, ...)  fac_call_fprintf(stderr, "%s:%d: " str "\n", __func__, __LINE__, ##__VA_ARGS__)

static void *xrealloc(void *p, size_t s) {
	void *pp = __libc_realloc(p, s);
	if (pp == NULL) {
		fac_disable();
		abort();
	}
	return pp;
}

#define strcmp2(a, b)  (strcmp(a, b) == 0)

/* ------------------------------------------------------------------------- */

struct call_s;

struct call_s {
	const char *name;
	size_t count;
};

struct calls_s {
	struct call_s *calls;
	size_t callscnt;
};

static struct calls_s g_calls_mem = {0};

static struct calls_s *const g_calls = &g_calls_mem;

#define CALLS_FOREACH(this, it) \
	for (struct call_s *it = &this->calls[0]; it != &this->calls[this->callscnt]; ++it)

static int calls_cmp_name(const void *aa, const void *bb) {
	const struct call_s *xx = aa;
	const struct call_s *yy = bb;
	return strcmp(xx->name, yy->name);
}

static int calls_find_name(const void *aa, const void *bb) {
	const char *name = aa;
	const struct call_s *yy = bb;
	return strcmp(name, yy->name);
}

static struct call_s *calls_find(const char *name) {
	struct calls_s *t = g_calls;
	if (t->calls == NULL) return NULL;
	return bsearch(name, t->calls, t->callscnt, sizeof(*t->calls), calls_find_name);
}

static struct call_s *calls_add(struct call_s newcall) {
	struct calls_s *t = g_calls;
	assert(newcall.name != NULL);
	assert(strlen(newcall.name) > 2);
	struct call_s *cc = calls_find(newcall.name);
	if (cc == NULL) {
		pdbg("allocating new { %s %d } %d", newcall.name, (int)newcall.count, (int)t->callscnt);
		t->calls = xrealloc(t->calls, ++t->callscnt * sizeof(*t->calls));
		t->calls[t->callscnt - 1] = newcall;
		qsort(t->calls, t->callscnt, sizeof(*t->calls), calls_cmp_name);
		cc = calls_find(newcall.name);
		assert(cc != NULL);
	}
	return cc;
}

static inline struct call_s *calls_set(struct call_s newcall) {
	struct call_s *cc = calls_add(newcall);
	*cc = newcall;
	return cc;
}

static struct call_s *calls_add_name(const char *name) {
	const struct call_s newcall = {
		.name = name,
		.count = (size_t)-1,
	};
	struct call_s *cc = calls_add(newcall);
	cc->count++;
	return cc;
}

static void calls_free() {
	struct calls_s *t = g_calls;
	__libc_free(t->calls);
	t->calls = NULL;
	t->callscnt = 0;
}

/* ------------------------------------------------------------------------- */

typedef bool call_cb(const struct call_s *cc, va_list va);

struct state_s {
	call_cb *callback;
	size_t curcount;
	size_t maxcount;
	size_t failoncount;
	bool not_counting;
	bool enabled;
};
static struct state_s g_state = {0};

void fac_atexit_callback(void) {
	fac_disable();
	calls_free();
}

struct libcnext_s {
	const char *name;
	void *func;
};

const struct libcnext_s libcnexts[] = {
	{ "malloc", __libc_malloc, },
	{ "realloc", __libc_realloc, },
	{ "free", __libc_free, },
};

static int libcnexts_find_name(const void *aa, const void *bb) {
	const char *name = aa;
	const struct libcnext_s *ln = bb;
	return strcmp(name, ln->name);
}


bool fac_interceptor_callback(const char *nextfuncname, void **next, const char *name, ...) {
	pdbg("not_counting=%d curcount=%d maxcout=%d enabled=%d failoncount=%d", g_state.not_counting, (int)g_state.curcount, (int)g_state.maxcount, g_state.enabled, (int)g_state.failoncount);
	if (*next == NULL) {
		const struct libcnext_s *ln = FIND_ARR(name, libcnexts, libcnexts_find_name);
		if (ln != NULL) {
			*next = ln->func;
		} else {
			*next = dlsym(RTLD_NEXT, nextfuncname);
		}
	}
	if (!g_state.enabled) {
		return false;
	}
	g_state.curcount++;
	if (g_state.curcount > g_state.maxcount) {
		g_state.maxcount = g_state.curcount;
	}
	if (g_calls->calls == NULL) {
		atexit(fac_atexit_callback);
	}
	struct call_s *cc = calls_add_name(name);
	if (g_state.callback != NULL) {
		va_list va;
		va_start(va, name);
		bool ret = g_state.callback(cc, va);
		va_end(va);
		return ret;
	}
	return false;
}

static void fac_enable(void) {
	g_state.enabled = true;
}

static void fac_disable(void) {
	g_state.enabled = false;
}

static bool fac_fail_callback(const struct call_s *cc, va_list va) {
	if (g_state.curcount != g_state.failoncount) return false;
	fac_failer_t *ff = fac_failer_find(cc->name);
	if (ff != NULL) {
		pdbg("failing %s", cc->name);
		ff(va);
		return true;
	}
	perr("failchecker: unhandled failure of function: %s\n", cc->name);
	return false;
}

unsigned long long fac_count_start(void) {
	pdbg("start counting");
	fac_enable();
	g_state.not_counting = false;
	g_state.callback = NULL;
	g_state.curcount = 0;
	g_state.maxcount = 0;
	calls_free();
	return 0;
}

bool fac_count_continue(unsigned long long count) {
	const bool ret = count == 0 || count - 1 < g_state.maxcount;
	if (ret == false) {
		pdbg("end loop");
	}
	return ret;
}

void fac_count_inc(unsigned long long count) {
	if (!g_state.not_counting) {
		pdbg("stop counting, start failing, cnt=%d count=%d", (int)count, (int)g_state.curcount);
		g_state.not_counting = true;
		g_state.callback = fac_fail_callback;
	}
	assert(count != 0);
	g_state.failoncount = count;
	g_state.curcount = 0;
}

/* ------------------------------------------------------------------------- */

#if 0
static struct calls_s g_failin = {0};


void fac_failin_malloc(size_t in) {
	const struct call_s newcall = {
		"malloc",
		in,
		fac_fail_voidp,
	};
	calls_set(g_failin, newcall);
	const struct call_s newcall2 = {
		"realloc",
		in,
		fac_fail_voidp,
	};
	calls_set(g_failin, newcall2);
}
#endif

/* ------------------------------------------------------------------------- */

#if 0

struct allocs {
	void *p;
	size_t s;
	bool freed;
	void *stack[20];
	size_t stackcnt;
};
static struct allocs *allocs = NULL;
static size_t allocscnt = 0;
static bool atexitadded = false;

static struct allocs *allocs_find(void *p) {
	for (size_t i = 0; i < allocscnt; ++i) {
		if (allocs[i].p == p) {
			return &allocs[i];
		}
	}
	return NULL;
}

static void allocs_rm(void *p) {
	struct allocs *this = allocs_find(p);
	if (this) {
		if (this + 1 != allocs + allocscnt) {
			memmove(this, this + 1, allocscnt - (this - allocs) - 1);
		}
		allocscnt--;
	}
}

static void allocs_add(void *p, size_t s) {
	if (p == NULL) return;
	allocs = xrealloc(allocs, ++allocscnt * sizeof(*allocs));
	struct allocs *const new = &allocs[allocscnt - 1];
	*new = (struct allocs) {
		.p = p,
		 .s = s,
	};
	new->stackcnt = backtrace(new->stack, sizeof(new->stack)/sizeof(*new->stack));
}

static void allocs_err(struct allocs *this, const char *str) {
	perr("%s\n", str);
	if (this == NULL) return;
	char **ss = backtrace_symbols(this->stack, this->stackcnt);
	for (size_t i = 0; i < this->stackcnt; ++i) {
		perr("%s\n", ss[i]);
	}
}

static void allocs_leaks(void) {
	for (size_t i = 0; i < allocscnt; ++i) {
		if (allocs[i].freed == false) {
			allocs_err(&allocs[i], "leaked");
		}
	}
}

/* ------------------------------------------------------------------------- */

enum fac_call_e {
	FAC_EMPTY,
	FAC_MALLOC,
	FAC_REALLOC,
	FAC_CNT,
};

struct call {
	enum fac_call_e call;
	unsigned long long cnt;
};

static struct call calls[FAC_CNT] = {0};
#define CALLSCNT  sizeof(calls)/sizeof(*calls)
static unsigned long long callscount = 0;
static unsigned long long callscountmax = 0;
static unsigned long long callfail = 0;

static bool call_add(enum fac_call_e call) {
	for (size_t i = 0; i < CALLSCNT; ++i) {
		if (calls[i].call == FAC_EMPTY) {
			calls[i].call = call;
		}
		if (calls[i].call == call) {
			calls[i].cnt++;
		}
	}
	callscount++;
	if (callfail) {
		if (callfail == callscount) {
			callfail = 0;
			return 1;
		}
	}
	return 0;
}

#if 0
static struct call *call_get(enum fac_call_e call) {
	for (size_t i = 0; i < CALLSCNT; ++i) {
		if (calls[i].call == FAC_EMPTY) {
			calls[i].call = call;
		}
		if (calls[i].call == call) {
			return &calls[i];
		}
	}
	assert(0);
	return NULL;
}
#endif

unsigned long long fac_get_count(void) {
	fprintf(stderr, "Getting count=%lld\n", callscount);
	return callscountmax;
}

void fac_set_fail(unsigned long long cnt) {
	fprintf(stderr, "Setting fail=%lld callscount=%lld\n", cnt, callscount);
	if (callscountmax == 0) {
		callscountmax = callscount;
	}
	callscount = 0;
	callfail = cnt;
}

/* ------------------------------------------------------------------------- */

static void failchecker_atexit(void) {
	allocs_leaks();
	__free(allocs);
	__free(calls);
}

void *fac_malloc(size_t s) {
	if (!atexitadded) {
		atexit(failchecker_atexit);
	}
	if (call_add(FAC_MALLOC)) return errno = ENOMEM, NULL;
	void *p = __libc_malloc(s);
	allocs_add(p, s);
	return p;
}

void *fac_realloc(void *p, size_t s) {
	if (!atexitadded) {
		atexit(failchecker_atexit);
	}
	if (call_add(FAC_MALLOC)) return errno = ENOMEM, NULL;
	allocs_rm(p);
	void *p2 = __libc_realloc(p, s);
	allocs_add(p2, s);
	return p2;
}

void fac_free(void *p) {
	if (p == NULL) return;
	struct allocs *this = allocs_find(p);
	if (this == NULL) {
		allocs_err(this, "pointer not allocated");
	} else {
		if (this->freed) {
			allocs_err(this, "double free");
		}
		this->freed = 1;
	}
	__free(p);
}

#endif

/* ------------------------------------------------------------------------- */

#define TEST(expr)  if(!(expr)) return -__LINE__

int fac_unittest(void) {
	calls_add_name("aaa1");
	calls_add_name("aaa2");
	calls_add_name("aaa3");
	calls_free();
	return 0;
}

