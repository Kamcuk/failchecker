#include "failchecker.h"
#include "failchecker_failers.h"
#include <asm-generic/errno-base.h>
#include <stdarg.h>
#include <stddef.h>
#include <errno.h>
#include <string.h>
#include <limits.h>
#include <assert.h>

struct failer_s {
	const char *name;
	int cnt;
	fac_failer_t *callback;
};

#define CONCAT(a, b)  a##b
#define XCONCAT(a, b)  CONCAT(a, b)

#define FAC_FAILER_ADD(name, cnt, func) \
	__attribute__((__used__)) \
	__attribute__((__section__("fac_failers")) \
	static const struct failer_s XCONCAT(fac_failer_, __LINE__) = { \
		#name, cnt, func \
	};


/* ------------------------------------------------------------------------- */

static void *fac_fail_malloc(size_t n) {
	errno = ENOMEM;
	return NULL;
}
FAC_FAILER_ADD(malloc)

static void *fac_fail_realloc(void *p, size_t n) {
	return fac_fail_malloc(n);
}
FAC_FAILER_ADD(realloc)

static int fac_fail_atoi(const char *str) {
	errno = ENOMEM;
	return 0;
}
FAC_FAILER_ADD(atoi)

// https://stackoverflow.com/questions/26080829/detecting-strtol-failure
static long fac_fail_strtol_1(const char *nptr, char **endptr, int base) {
	*endptr = (char *)nptr; return 0;
}
static long fac_fail_strtol_2(const char *nptr, char **endptr, int base) {
	errno = ERANGE; return LONG_MIN;
}
static long fac_fail_strtol_3(const char *nptr, char **endptr, int base) {
	errno = ERANGE; return LONG_MAX;
}
static long fac_fail_strtol_4(const char *nptr, char **endptr, int base) {
	errno = EINVAL;
	return 0;
}
static long fac_fail_strtol_5(const char *nptr, char **endptr, int base) {
	errno = ENOMEM;
	return 0;
}

static void fac_fail_mbrtowc(int cnt, va_list va) {
	if (cnt == -1) *va_arg(va, int*) = 2;
	size_t *ret = va_arg(va, size_t*);
	switch (cnt) {
	case 0: *ret = -1; errno = EILSEQ; break;
	case 1: *ret = -2; break;
	default: assert(0);
	}
}

/* ------------------------------------------------------------------------- */

const struct failer_s g_failers[] = {
	{ "malloc",  fac_fail_enomem, },
	{ "realloc", fac_fail_enomem, },
	{ "atoi",    fac_fail_atoi,   },
	{ "strtol",  fac_fail_strtol, },
	{ "mbrtowc", fac_fail_mbrtowc, },
};

static int failer_find_name(const void *aa, const void *bb) {
	const char *name = aa;
	const struct failer_s *ff = bb;
	return strcmp(name, ff->name);
}

static void fac_failer_get_cnt_in(fac_failer_t ff, ...) {
	va_list va;
	va_start(va, ff);
	ff(-1, va);
	va_end(va);
}

static int fac_failer_get_cnt(fac_failer_t ff) {
	int cnt = 0;
	fac_failer_get_cnt_in(ff, cnt);
	return cnt == 0 ? 1 : cnt;
}

fac_failer_t *fac_find_failer(const char *name) {
	const struct failer_s *ff = FIND_ARR(name, g_failers, failer_find_name);
	return ff != NULL ? ff->callback : NULL;
}
