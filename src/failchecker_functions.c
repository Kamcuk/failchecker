#include "failchecker.h"
#include <stdarg.h>
#include <time.h>
#include <stdio.h>

#define FAC_ESC(...)  __VA_ARGS__

#define FAC_OVERLOAD_N(PREFIX, _5,_4,_3,_2,_1,_0,N,...) \
		PREFIX ## _ ## N
#define FAC_OVERLOAD(PREFIX, ...) \
		FAC_OVERLOAD_N(PREFIX, 0, ## __VA_ARGS__, 5,4,3,2,1,0)(__VA_ARGS__)

#define FAC_ARGS_TO_ARGSNAMES_0()                void
#define FAC_ARGS_TO_ARGSNAMES_1(_1)              _1 a
#define FAC_ARGS_TO_ARGSNAMES_2(_1,_2)           _1 a, _2 b
#define FAC_ARGS_TO_ARGSNAMES_3(_1,_2,_3)        _1 a, _2 b, _3 c
#define FAC_ARGS_TO_ARGSNAMES_4(_1,_2,_3,_4)     _1 a, _2 b, _3 c, _4 d
#define FAC_ARGS_TO_ARGSNAMES_5(_1,_2,_3,_4,_5)  _1 a, _2 b, _3 c, _4 d, _5 e
#define FAC_ARGS_TO_ARGSNAMES(...)  \
		FAC_OVERLOAD(FAC_ARGS_TO_ARGSNAMES, ##__VA_ARGS__)

#define FAC_ARGS_TO_ARGSVALS_0()
#define FAC_ARGS_TO_ARGSVALS_1(_1)              a
#define FAC_ARGS_TO_ARGSVALS_2(_1,_2)           a, b
#define FAC_ARGS_TO_ARGSVALS_3(_1,_2,_3)        a, b, c
#define FAC_ARGS_TO_ARGSVALS_4(_1,_2,_3,_4)     a, b, c, d
#define FAC_ARGS_TO_ARGSVALS_5(_1,_2,_3,_4,_5)  a, b, c, d, e
#define FAC_ARGS_TO_ARGSVALS(...)  \
		FAC_OVERLOAD(FAC_ARGS_TO_ARGSVALS, ##__VA_ARGS__)

#define FAC_LASTARG_0()
#define FAC_LASTARG_1(_1)              a
#define FAC_LASTARG_2(_1,_2)           b
#define FAC_LASTARG_3(_1,_2,_3)        c
#define FAC_LASTARG_4(_1,_2,_3,_4)     d
#define FAC_LASTARG_5(_1,_2,_3,_4,_5)  e
#define FAC_LASTARG(...)  \
		FAC_OVERLOAD(FAC_LASTARG, ##__VA_ARGS__)

#define FAC_COMMA_0()
#define FAC_COMMA_1(_1)             ,
#define FAC_COMMA_2(_1,_2)          ,
#define FAC_COMMA_3(_1,_2,_3)       ,
#define FAC_COMMA_4(_1,_2,_3,_4)    ,
#define FAC_COMMA_5(_1,_2,_3,_4,_5) ,
#define FAC_COMMA(...)  \
		FAC_OVERLOAD(FAC_COMMA, ##__VA_ARGS__)

#define FAC_VA_ARG_0()
#define FAC_VA_ARG_1(_1)             _1 a = va_arg(va, _1)
#define FAC_VA_ARG_2(_1,_2)          FAC_VA_ARG_1(_1); _2 b = va_arg(va, _2)
#define FAC_VA_ARG_3(_1,_2,_3)       FAC_VA_ARG_2(_1,_2); _3 c = va_arg(va, _3)
#define FAC_VA_ARG_4(_1,_2,_3,_4)    FAC_VA_ARG_3(_1,_2,_3); _4 c = va_arg(va, _4)
#define FAC_VA_ARG_5(_1,_2,_3,_4,_5) FAC_VA_ARG_4(_1,_2,_3,_4); _5 c = va_arg(va, _5)
#define FAC_VA_ARG(...)  \
		FAC_OVERLOAD(FAC_VA_ARG, ##__VA_ARGS__)

#define FAC_DEFINE(RET, NAME, ...)  \
		FAC_DEFINE_IN(RET, NAME, \
				( FAC_COMMA(__VA_ARGS__) ), \
				( FAC_ARGS_TO_ARGSNAMES(__VA_ARGS__) ), \
				( FAC_ARGS_TO_ARGSVALS(__VA_ARGS__) ))
#define FAC_DEFINE_IN(RET, NAME, COMMA, ARGSNAMES, ARGSVALS) \
RET NAME ARGSNAMES { \
	static void *next = 0; \
	RET ret = {0}; \
	if (fac_interceptor_callback(#NAME, &next, #NAME, &ret FAC_ESC COMMA FAC_ESC ARGSVALS )) return ret; \
	RET (*const fnext) ARGSNAMES = next; \
	ret = (*fnext) ARGSVALS ; \
	return ret; \
}

#define FAC_DEFINE_VA_ARGS(RET, NAME, ...)  \
		FAC_DEFINE_VA_ARGS_IN(RET, NAME, \
				FAC_LASTARG(__VA_ARGS__), \
				( FAC_COMMA(__VA_ARGS__) ), \
				( FAC_ARGS_TO_ARGSNAMES(__VA_ARGS__) ), \
				( FAC_ARGS_TO_ARGSVALS(__VA_ARGS__) ))
#define FAC_DEFINE_VA_ARGS_IN(RET, NAME, LASTARG, COMMA, ARGSNAMES, ARGSVALS) \
RET NAME ( FAC_ESC ARGSNAMES, ...) { \
	static void *next = 0; \
	RET ret = {0}; \
	va_list va; \
	va_start(va, LASTARG); \
	if (fac_interceptor_callback("v" #NAME, &next, #NAME, &ret FAC_ESC COMMA FAC_ESC ARGSVALS , va )) return ret; \
	va_end(va); \
	va_start(va, LASTARG); \
	RET (*const fnext)( FAC_ESC ARGSNAMES, va_list) = next; \
	ret = (*fnext)( FAC_ESC ARGSVALS , va ); \
	va_end(va); \
	return ret; \
}

#define FAC_DEFINE2(RET, NAME, ...) \
		FAC_DEFINE2_IN(RET, NAME, \
				FAC_VA_ARG(__VA_ARGS__), \
				( FAC_COMMA(__VA_ARGS__) ), \
				( FAC_ARGS_TO_ARGSNAMES(__VA_ARGS__) ), \
				( FAC_ARGS_TO_ARGSVALS(__VA_ARGS__) ))
#define FAC_DEFINE2_IN(RET, NAME, SET_ARGS, COMMA, ARGSNAMES, ARGSVALS) \
void fac_dofail_##NAME (va_list va) { \
	RET *l_ret = va_arg(va, RET *); \
	SET_ARGS; \
	*l_ret = fac_fail_##NAME ARGSVALS ; \
}


#include <failchecker_functions_gen.h>

