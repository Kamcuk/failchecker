
#include <search.h>

#define COUNT(arr)  (sizeof(arr)/sizeof(*arr))

#define FIND_ARR(key, arr, cmp)  \
	lfind(key, arr, (size_t[1]){ COUNT(arr) }, sizeof(*arr), cmp);
