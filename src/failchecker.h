
/*
 * @file
 * @author Kamil Cukrowski <kamil@leonidas>
 * @date 2022-04-28
 * SPDX-License-Identifier: MIT AND Beerware
 */
#ifndef FAILCHECKER_H_
#define FAILCHECKER_H_
#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include <stdbool.h>
#include <stdarg.h>

typedef void fac_failer_t(int cnt, va_list va);

fac_failer_t *fac_failer_find(const char *name);

bool fac_interceptor_callback(const char *nextfuncname, void **next, const char *funcname, ...);

unsigned long long fac_count_start(void);
bool fac_count_continue(unsigned long long count);
void fac_count_inc(unsigned long long count);

int fac_unittest(void);

#define FAC_FOR(ok) \
	for (unsigned long long ok = fac_count_start(); fac_count_continue(ok); fac_count_inc(++ok))

#ifdef __cplusplus
}
#endif
#endif /* FAILCHECKER_H_ */
